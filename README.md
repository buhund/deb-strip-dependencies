# deb-strip-dependencies

Modify .deb dependencies.





## Snipped from OP at [ubuntuforums](https://ubuntuforums.org/showthread.php?t=636724) by user Loevborg:


Save the following script as "videbcontrol",
Code:

    chmod +x

it and put it in your path. Then by calling:

Code:

    videbcontrol foo.deb

you can edit the "Depends:" line. The resulting .deb file is written to foo.modified.deb in the current directory.

Code:

    #!/bin/bash
    
    if [[ -z "$1" ]]; then
      echo "Syntax: $0 debfile"
      exit 1
    fi
    
    DEBFILE="$1"
    TMPDIR=`mktemp -d /tmp/deb.XXXXXXXXXX` || exit 1
    OUTPUT=`basename "$DEBFILE" .deb`.modfied.deb
    
    if [[ -e "$OUTPUT" ]]; then
      echo "$OUTPUT exists."
      rm -r "$TMPDIR"
      exit 1
    fi
    
    dpkg-deb -x "$DEBFILE" "$TMPDIR"
    dpkg-deb --control "$DEBFILE" "$TMPDIR"/DEBIAN
    
    if [[ ! -e "$TMPDIR"/DEBIAN/control ]]; then
      echo DEBIAN/control not found.
    
      rm -r "$TMPDIR"
      exit 1
    fi
    
    CONTROL="$TMPDIR"/DEBIAN/control
    
    MOD=`stat -c "%y" "$CONTROL"`
    vi "$CONTROL"
    
    if [[ "$MOD" == `stat -c "%y" "$CONTROL"` ]]; then
      echo Not modfied.
    else
      echo Building new deb...
      dpkg -b "$TMPDIR" "$OUTPUT"
    fi
    
    rm -r "$TMPDIR"

PS: Thanks to http://ubuntuforums.org/showthread.php?t=110458


## Also, the post referenced in the first quoted piece, [posted by Ubuntuforum user bean1975](http://ubuntuforums.org/showthread.php?t=110458):

While installing monkey audio .deb files from http://www.rarewares.org I found that they failed a minor dependency libstdc++6 (>= 4.0.2) was needed while I had 4.0.1-4ubuntu9, so I decided that I'll try whether it works -- the difference seemed minor. Warning: this may be dangerous to your system. You may end up with a system that is unrepairable. You are off all tracks and on your own if you try this. LIkely apt will awake Godzilla to eat you .

With that said, dependency information is stored in a file called control. Follow the following simple steps;

    dpkg-deb -x foo.deb tmpdir
    dpkg-deb --control foo.deb tmpdir/DEBIAN
    nano tmpdir/DEBIAN/control
    dpkg -b tmpdir hacked.deb



and there you are, your shiny new .deb file. And yes, monkey audio works.


_____

### Credits and sources:

- https://ubuntuforums.org/showthread.php?t=636724
- https://gist.github.com/ggiovinazzo/ed7bc85b3c6797f318d228298cc93ea2
- https://github.com/fcole90/fah-control/releases/tag/7.5.1-1_focal_1
- - Thanks to this script I could modify the dependencies required by FAHControl and python-gtk2 in focal. I also modified the interpreter shebang to point to `/usr/bin/env python2` so it can find the right interpreter.
If you run it and it can't find the client, you can try to run `FAHClient` from terminal and it should run and detect it.
- https://ubuntuforums.org/showthread.php?t=636724
- http://ubuntuforums.org/showthread.php?t=110458